<?php

declare(strict_types=1);

namespace Zalmoksis\User\Tests\Unit\Proxy;

use PHPUnit\Framework\{MockObject\MockObject, TestCase};
use Zalmoksis\User\{Proxy\UserProxyRepository, User, UserRepository};

class UserProxyRepositoryTest extends TestCase {
    private UserProxyRepository $proxyRepository;

    private UserRepository | MockObject $innerRepository;

    function setUp(): void {
        $this->innerRepository = $this->createMock(UserRepository::class);
        $this->proxyRepository = new UserProxyRepository($this->innerRepository);
    }

    function tesIfImplementsUserRepository(): void {
        $this->assertInstanceOf(UserRepository::class, $this->proxyRepository);
    }

    function testSavingAndFindingCached(): void {
        $this->innerRepository
            ->expects($this->once())
            ->method('save')
            ->with(new User('user_1', ['role_1', 'role_2']));
        $this->innerRepository
            ->expects($this->never())
            ->method('findByLogin');

        $this->proxyRepository->save(new User('user_1', ['role_1', 'role_2']));
        $this->assertEquals(
            new User('user_1', ['role_1', 'role_2']),
            $this->proxyRepository->findByLogin('user_1'),
        );
    }

    function testUpdatingAndFindingCached(): void {
        $this->innerRepository
            ->expects($this->once())
            ->method('save')
            ->with(new User('user_1', ['role_1', 'role_2']));
        $this->innerRepository
            ->expects($this->once())
            ->method('update')
            ->with(new User('user_1', ['role_3']));
        $this->innerRepository
            ->expects($this->never())
            ->method('findByLogin');

        $this->proxyRepository->save(new User('user_1', ['role_1', 'role_2']));
        $this->proxyRepository->update(new User('user_1', ['role_3']));
        $this->assertEquals(
            new User('user_1', ['role_3']),
            $this->proxyRepository->findByLogin('user_1'),
        );
    }

    function testFindingNotCachedButStored(): void {
        $this->innerRepository
            ->expects($this->once())
            ->method('findByLogin')
            ->with('user_1')
            ->willReturn(
                new User('user_1', ['role_1', 'role_2'])
            );

        $this->assertEquals(
            new User('user_1', ['role_1', 'role_2']),
            $this->proxyRepository->findByLogin('user_1'),
        );

        // cached the second time
        $this->assertEquals(
            new User('user_1', ['role_1', 'role_2']),
            $this->proxyRepository->findByLogin('user_1'),
        );
    }

    function testFindingWhenNotCachedAndNotStored(): void {
        $this->innerRepository
            ->expects($this->once())
            ->method('findByLogin')
            ->with('user_1')
            ->willReturn(null);

        $this->assertNull(
            $this->proxyRepository->findByLogin('user_1')
        );

        // cached the second time
        $this->assertNull(
            $this->proxyRepository->findByLogin('user_1')
        );
    }

    function testDeletingWhenNotCached(): void {
        $this->innerRepository
            ->expects($this->once())
            ->method('deleteByLogin')
            ->with('user_1');
        $this->innerRepository
            ->expects($this->never())
            ->method('findByLogin')
            ->willReturn(null);

        $this->proxyRepository->deleteByLogin('user_1');
        $this->assertNull($this->proxyRepository->findByLogin('user_1'));
    }

    function testDeletingWhenCached(): void {
        $this->innerRepository
            ->expects($this->once())
            ->method('deleteByLogin')
            ->with('user_1');
        $this->innerRepository
            ->expects($this->never())
            ->method('findByLogin');

        $this->proxyRepository->save(new User('user_1', ['role_1', 'role_2']));
        $this->proxyRepository->deleteByLogin('user_1');
        $this->assertNull($this->proxyRepository->findByLogin('user_1'));
    }
}
