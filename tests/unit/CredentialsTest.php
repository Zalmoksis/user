<?php

declare(strict_types=1);

namespace Zalmoksis\User\Tests\Unit;

use PHPUnit\Framework\TestCase;
use Zalmoksis\User\Credentials;

class CredentialsTest extends TestCase {

    function testGetters(): void {
        $credentials = new Credentials('user', 'password');

        $this->assertEquals('user', $credentials->getLogin());
        $this->assertEquals('password', $credentials->getPassword());
    }

    function testChangingPassword(): void {
        $credentials = new Credentials('user', 'password');
        $changedCredentials = $credentials->changePassword('changedPassword');

        $this->assertEquals('user', $changedCredentials->getLogin());
        $this->assertEquals('changedPassword', $changedCredentials->getPassword());
    }

    function testIfChangingPasswordIsImmutable(): void {
        $credentials = new Credentials('user', 'password');
        $changedCredentials = $credentials->changePassword('changedPassword');

        $this->assertNotEquals($credentials, $changedCredentials);
    }
}
