<?php

declare(strict_types=1);

namespace Zalmoksis\User\Tests\Unit;

use Zalmoksis\User\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase {

    function testGetters(): void {
        $user = new User('user', ['role_1', 'role_2']);

        $this->assertSame('user', $user->getLogin());
        $this->assertSame(['role_1', 'role_2'], $user->getRoles());
        $this->assertTrue($user->hasRole('role_2'));
        $this->assertFalse($user->hasRole('role_3'));
    }
}
