<?php

declare(strict_types=1);

namespace Zalmoksis\User\Tests\Unit;

use PHPUnit\Framework\TestCase;
use Zalmoksis\User\HashedCredentials;

class HashedCredentialsTest extends TestCase {

    function testGetters(): void {
        $hashedCredentials = new HashedCredentials('user', 'passwordHash');

        $this->assertEquals('user', $hashedCredentials->getLogin());
        $this->assertEquals('passwordHash', $hashedCredentials->getPasswordHash());
    }

    function testChangingPassword(): void {
        $hashedCredentials = new HashedCredentials('user', 'passwordHash');
        $changedHashedCredentials = $hashedCredentials->changePasswordHash('changedPasswordHash');

        $this->assertEquals('user', $changedHashedCredentials->getLogin());
        $this->assertEquals('changedPasswordHash', $changedHashedCredentials->getPasswordHash());
    }

    function testIfChangingPasswordIsImmutable(): void {
        $hashedCredentials = new HashedCredentials('user', 'passwordHash');
        $changedHashedCredentials = $hashedCredentials->changePasswordHash('changedPasswordHash');

        $this->assertNotEquals($hashedCredentials, $changedHashedCredentials);
    }
}
