<?php

/** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

namespace Zalmoksis\User\Tests\Functional;

use MongoDB\Driver\{Command, Manager as Mongo};
use PHPUnit\Framework\{MockObject\MockObject, TestCase};
use Zalmoksis\User\{Authenticator, Credentials, Session, User, UserManager};
use Zalmoksis\User\Mongo\{CredentialsMongoRepository, UserMongoRepository};

final class UserManagerTest extends TestCase {
    private Mongo $mongo;
    private UserManager $userManager;

    private Session | MockObject $session;

    private const DATABASE = 'functional_tests_of_user_manager';

    function setUp(): void {
        $mongoHost = getenv('MONGO_HOST') ?: 'localhost';
        $mongoPort = getenv('MONGO_PORT') ?: '27017';

        $this->session = $this->createMock(Session::class);

        $this->mongo = new Mongo("mongodb://{$mongoHost}:{$mongoPort}");
        $this->userManager = new UserManager(
            new Authenticator(
                new CredentialsMongoRepository($this->mongo, self::DATABASE)
            ),
            $this->session,
            new UserMongoRepository($this->mongo, self::DATABASE),
        );

        $this->dropDatabase();
        $this->userManager->addUser(
            new Credentials('user', 'password'),
            new User('user', ['role_1', 'role_2']),
        );
    }

    function tearDown(): void {
        $this->dropDatabase();
    }

    function testLoggingInWithCorrectCredentials(): void {
        $this->assertTrue($this->userManager->logIn(new Credentials('user', 'password')));
    }

    function testLoggingInWithIncorrectPassword(): void {
        $this->assertFalse($this->userManager->logIn(new Credentials('user', 'password2')));
    }

    function testLoggingInWithIncorrectLogin(): void {
        $this->assertFalse($this->userManager->logIn(new Credentials('user2', 'password')));
    }

    function testGettingAuthenticatedUser(): void {
        $this->session->method('has')->with('authenticated_user')->willReturn(true);
        $this->session->method('get')->with('authenticated_user')->willReturn('user');

        $this->assertEquals(
            new User('user', ['role_1', 'role_2']),
            $this->userManager->getAuthenticatedUser(),
        );
    }

    function testUpdatingCredentials(): void {
        $this->userManager->updateCredentials(new Credentials('user', 'password2'));

        $this->assertTrue($this->userManager->logIn(new Credentials('user', 'password2')));
    }

    function testUpdatingUser(): void {
        $this->session->method('has')->with('authenticated_user')->willReturn(true);
        $this->session->method('get')->with('authenticated_user')->willReturn('user');

        $this->userManager->updateUser(new User('user', ['role_2', 'role_3']));

        $this->assertEquals(
            new User('user', ['role_2', 'role_3']),
            $this->userManager->getAuthenticatedUser(),
        );
    }

    function testLoggingOut(): void {
        $this->session->expects($this->once())->method('destroy');

        $this->userManager->logOut();
    }

    private function dropDatabase(): void {
        $this->mongo->executeCommand(self::DATABASE, new Command(['dropDatabase' => 1]));
    }
}
