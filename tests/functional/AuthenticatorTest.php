<?php

/** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

namespace Zalmoksis\User\Tests\Functional;

use MongoDB\Driver\{Command, Manager as Mongo};
use PHPUnit\Framework\TestCase;
use Zalmoksis\User\{Authenticator, Credentials, Exceptions\DuplicateLogin, Mongo\CredentialsMongoRepository};

final class AuthenticatorTest extends TestCase {
    private Mongo $mongo;
    private Authenticator $authenticator;

    private const DATABASE = 'functional_tests_of_authenticator';

    function setUp(): void {
        $mongoHost = getenv('MONGO_HOST') ?: 'localhost';
        $mongoPort = getenv('MONGO_PORT') ?: '27017';

        $this->mongo = new Mongo("mongodb://{$mongoHost}:{$mongoPort}");
        $this->authenticator = new Authenticator(
            new CredentialsMongoRepository($this->mongo, self::DATABASE)
        );
        $this->dropDatabase();

        // default user and password
        $this->authenticator->storeCredentials(new Credentials('user', 'password'));
    }

    function tearDown(): void {
        $this->dropDatabase();
    }

    function testSavingWhenIdentityAlreadyExists(): void {
        $this->expectException(DuplicateLogin::class);
        $this->authenticator->storeCredentials(new Credentials('user', 'password'));
    }

    function testAuthenticationWithCorrectPassword(): void {
        $this->assertTrue($this->authenticator->authenticate(new Credentials('user', 'password')));
    }

    function testAuthenticationWithIncorrectUser(): void {
        $this->assertFalse($this->authenticator->authenticate(new Credentials('user1', 'password')));
        $this->assertFalse($this->authenticator->authenticate(new Credentials('User', 'password')));
        $this->assertFalse($this->authenticator->authenticate(new Credentials('', 'password')));
    }

    function testAuthenticationWithIncorrectPassword(): void {
        $this->assertFalse($this->authenticator->authenticate(new Credentials('user', 'password1')));
        $this->assertFalse($this->authenticator->authenticate(new Credentials('user', 'Password')));
        $this->assertFalse($this->authenticator->authenticate(new Credentials('user', '')));
    }

    function testAuthenticationAfterPasswordChange(): void {
        $this->authenticator->updateCredentials(new Credentials('user', 'new_password'));
        $this->assertFalse($this->authenticator->authenticate(new Credentials('user', 'password')));
        $this->assertTrue($this->authenticator->authenticate(new Credentials('user', 'new_password')));
    }

    private function dropDatabase(): void {
        $this->mongo->executeCommand(self::DATABASE, new Command(['dropDatabase' => 1]));
    }
}
