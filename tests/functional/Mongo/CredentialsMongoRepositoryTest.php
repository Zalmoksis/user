<?php

/** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

namespace Zalmoksis\User\Tests\Functional\Mongo;

use MongoDB\Driver\{BulkWrite, Command, Manager as Mongo};
use Iterator;
use PHPUnit\Framework\TestCase;
use Zalmoksis\User\{
    Exceptions\CorruptedDocument,
    Exceptions\DuplicateLogin,
    HashedCredentials,
    CredentialsRepository,
    Mongo\CredentialsMongoRepository,
};

final class CredentialsMongoRepositoryTest extends TestCase {
    private Mongo $mongo;
    private CredentialsMongoRepository $repository;

    private const DATABASE = 'functional_tests_of_credentials_repository';

    function setUp(): void {
        $mongoHost = getenv('MONGO_HOST') ?: 'localhost';
        $mongoPort = getenv('MONGO_PORT') ?: '27017';

        $this->mongo = new Mongo("mongodb://{$mongoHost}:{$mongoPort}");
        $this->repository = new CredentialsMongoRepository($this->mongo, self::DATABASE);

        $this->dropDatabase();
    }

    function tearDown(): void {
        $this->dropDatabase();
    }

    function testIfInstanceOfCredentialsRepository(): void {
        $this->assertInstanceOf(CredentialsRepository::class, $this->repository);
    }

    function testSavingTwiceTheSameUser(): void {
        $this->expectException(DuplicateLogin::class);
        $this->expectExceptionMessage(
            "Duplicate login 'user' in 'functional_tests_of_credentials_repository.user_credentials'"
        );

        $this->repository->save(new HashedCredentials('user', 'passwordHash'));
        $this->repository->save(new HashedCredentials('user', 'passwordHash'));
    }

    function testFindingSavedCredentials(): void {
        $this->repository->save(new HashedCredentials('user', 'passwordHash'));

        $this->assertEquals(
            new HashedCredentials('user', 'passwordHash'),
            $this->repository->findByLogin('user')
        );
    }

    function testIfFindingCredentialsThatWereNotSavedReturnsNothing(): void {
        $this->assertNull($this->repository->findByLogin('user'));
    }

    function testFindingUpdatedCredentials(): void {
        $this->repository->save(new HashedCredentials('user', 'passwordHash'));
        $this->repository->update(new HashedCredentials('user', 'differentPasswordHash'));

        $this->assertEquals(
            new HashedCredentials('user', 'differentPasswordHash'),
            $this->repository->findByLogin('user')
        );
    }

    function provideCorruptedDocuments(): Iterator {
        yield ['document' => ['_id' => 'user']];
        yield ['document' => ['_id' => 'user', 'hashed_password' => []]];
    }

    /** @dataProvider provideCorruptedDocuments */
    function testFindingCorruptedUserDocument(array $document): void {
        $this->expectException(CorruptedDocument::class);
        $this->expectExceptionMessage(
            "Corrupted document with _id: 'user' in 'functional_tests_of_credentials_repository.user_credentials'"
        );

        $this->insert($document);
        $this->repository->findByLogin('user');
    }

    function testUpdatingNonexistentUser(): void {
        // TODO: nothing happens here; should it?
        $this->repository->update(new HashedCredentials('differentUser', 'differentPasswordHash'));
        $this->assertNull($this->repository->findByLogin('differentUser'));
    }

    function testDeletingExistingUser(): void {
        $this->repository->save(new HashedCredentials('user', 'passwordHash'));
        $this->repository->delete(new HashedCredentials('user', 'passwordHash'));

        $this->assertNull($this->repository->findByLogin('user'));
    }

    function testDeletingNonexistentUser(): void {
        $this->repository->delete(new HashedCredentials('user', 'passwordHash'));

        $this->assertNull($this->repository->findByLogin('user'));
    }

    private function dropDatabase(): void {
        $this->mongo->executeCommand(self::DATABASE, new Command(['dropDatabase' => 1]));
    }

    private function insert(array $document): void {
        $bulkWrite = new BulkWrite();
        $bulkWrite->insert($document);
        $this->mongo->executeBulkWrite(self::DATABASE . '.user_credentials', $bulkWrite);
    }
}
