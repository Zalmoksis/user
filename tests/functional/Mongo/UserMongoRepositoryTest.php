<?php

/**
 * @noinspection PhpDocSignatureInspection
 * @noinspection PhpUnhandledExceptionInspection
 */

declare(strict_types=1);

namespace Zalmoksis\User\Tests\Functional\Mongo;

use MongoDB\Driver\{BulkWrite, Command, Manager as Mongo};
use Iterator;
use PHPUnit\Framework\TestCase;
use Zalmoksis\User\{
    Exceptions\CorruptedDocument,
    Exceptions\DuplicateLogin,
    Mongo\UserMongoRepository,
    User,
    UserRepository,
};

final class UserMongoRepositoryTest extends TestCase {
    private Mongo $mongo;
    private UserMongoRepository $repository;

    private const DATABASE = 'functional_tests_of_user_repository';

    function setUp(): void {
        $mongoHost = getenv('MONGO_HOST') ?: 'localhost';
        $mongoPort = getenv('MONGO_PORT') ?: '27017';

        $this->mongo = new Mongo("mongodb://{$mongoHost}:{$mongoPort}");
        $this->repository = new UserMongoRepository($this->mongo, self::DATABASE);

        $this->dropDatabase();
    }

    function tearDown(): void {
        $this->dropDatabase();
    }

    function testIfInstanceOfCredentialsRepository(): void {
        $this->assertInstanceOf(UserRepository::class, $this->repository);
    }

    function testSavingTwiceTheSameUser(): void {
        $this->expectException(DuplicateLogin::class);
        $this->expectExceptionMessage(
            "Duplicate login 'user' in 'functional_tests_of_user_repository.users'"
        );

        $this->repository->save(new User('user'));
        $this->repository->save(new User('user'));
    }

    function provideUsers(): Iterator {
        yield ['user' => new User('user')];
        yield ['user' => new User('user', ['role1', 'role2'])];
    }

    /** @dataProvider provideUsers */
    function testFindingSavedUser(User $user): void {
        $this->repository->save(clone $user);

        $this->assertEquals(
            clone $user,
            $this->repository->findByLogin('user')
        );
    }

    function testIfFindingUserThatWasNotSavedReturnsNothing(): void {
        $this->assertNull($this->repository->findByLogin('user'));
    }

    function testFindingUpdatedUser(): void {
        $this->repository->save(new User('user', ['role_1', 'role_2']));
        $this->repository->update(new User('user', ['role_1', 'role_3']));

        $this->assertEquals(
            new User('user', ['role_1', 'role_3']),
            $this->repository->findByLogin('user')
        );
    }

    function provideCorruptedDocuments(): Iterator {
        yield ['document' => ['_id' => 'user', 'roles' => 'role_1']];
        yield ['document' => ['_id' => 'user', 'roles' => ['role_1', []]]];
    }

    /** @dataProvider provideCorruptedDocuments */
    function testFindingCorruptedUserDocument(array $document): void {
        $this->expectException(CorruptedDocument::class);
        $this->expectExceptionMessage(
            "Corrupted document with _id: 'user' in 'functional_tests_of_user_repository.users'"
        );

        $this->insert($document);
        $this->repository->findByLogin('user');
    }

    function testUpdatingNonexistentUser(): void {
        // TODO: nothing happens here; should it?
        $this->repository->update(new User('differentUser', ['role_1', 'role_3']));

        $this->assertNull($this->repository->findByLogin('differentUser'));
    }

    function testDeletingExistingUser(): void {
        $this->repository->save(new User('user', ['role_1', 'role_2']));
        $this->repository->deleteByLogin('user');

        $this->assertNull($this->repository->findByLogin('user'));
    }

    function testDeletingNonexistentUser(): void {
        $this->repository->deleteByLogin('user');

        $this->assertNull($this->repository->findByLogin('user'));
    }

    private function dropDatabase(): void {
        $this->mongo->executeCommand(self::DATABASE, new Command(['dropDatabase' => 1]));
    }

    private function insert(array $document): void {
        $bulkWrite = new BulkWrite();
        $bulkWrite->insert($document);
        $this->mongo->executeBulkWrite(self::DATABASE . '.users', $bulkWrite);
    }
}
