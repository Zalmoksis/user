<?php

declare(strict_types=1);

namespace Zalmoksis\User;

class User {
    function __construct(
        private string $login,
        private array $roles = [],
    ) {}

    function getLogin(): string {
        return $this->login;
    }

    function getRoles(): array {
        return $this->roles;
    }

    function hasRole(string $role): bool {
        return in_array($role, $this->roles);
    }
}
