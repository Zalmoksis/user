<?php

declare(strict_types=1);

namespace Zalmoksis\User\Php;

use Zalmoksis\User\Session;

final class PhpSession implements Session {
    function start(): self {
        session_start();

        return $this;
    }

    function set(string $key, mixed $value): self {
        $_SESSION[$key] = $value;

        return $this;
    }

    function has(string $key): bool {
        return isset($_SESSION[$key]);
    }

    function get(string $key): mixed {
        return $_SESSION[$key];
    }

    function destroy(): self {
        $this->destroyCookie();
        session_destroy();

        return $this;
    }

    private function destroyCookie(): void {
        if (ini_get('session.use_cookies')) {
            $params = session_get_cookie_params();
            setcookie(
                session_name(),
                '',
                time() - 42000, // TODO: Magick!
                $params['path'],
                $params['domain'],
                $params['secure'],
                $params['httponly'],
            );
        }
    }
}
