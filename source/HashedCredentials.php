<?php

declare(strict_types=1);

namespace Zalmoksis\User;

final class HashedCredentials {
    function __construct(
        private string $login,
        private string $passwordHash,
    ) {}

    function getLogin(): string {
        return $this->login;
    }

    function getPasswordHash(): string {
        return $this->passwordHash;
    }

    function changePasswordHash(string $passwordHash): self {
        return new self($this->getLogin(), $passwordHash);
    }
}
