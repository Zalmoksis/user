<?php

declare(strict_types=1);

namespace Zalmoksis\User\Mongo;

use MongoDB\Driver\Query;
use Zalmoksis\User\{CredentialsRepository,
    Exceptions\CorruptedDocument,
    Exceptions\DuplicateKey,
    Exceptions\DuplicateLogin,
    HashedCredentials as Credentials};

final class CredentialsMongoRepository extends MongoRepository implements CredentialsRepository {
    protected const COLLECTION = 'user_credentials';

    private const FIELD_ID              = '_id';
    private const FIELD_HASHED_PASSWORD = 'hashed_password';

    function save(Credentials $credentials): void {
        try {
            $this->insertDocument(
                $this->serializeCredentials($credentials)
            );
        } catch (DuplicateKey $exception) {
            throw new DuplicateLogin(
                login: $credentials->getLogin(),
                namespace: $exception->getNamespace(),
                previous: $exception
            );
        }
    }

    function update(Credentials $credentials): void {
        $this->updateDocument(
            $this->serializeCredentialsPartWithId($credentials),
            $this->serializeCredentials($credentials),
        );
    }

    function findByLogin(string $login): ?Credentials {
        $cursor = $this->executeQuery(
            new Query($this->createDocumentPartWithLogin($login))
        );

        /** @var array | null $credentialsDocument */
        $credentialsDocument = $cursor->toArray()[0] ?? null;

        if (null === $credentialsDocument) {
            return null;
        }

        if (!$this->isCredentialsDocumentValid($credentialsDocument)) {
            throw new CorruptedDocument(
                document: $credentialsDocument,
                namespace: $this->database . '.' . self::COLLECTION,
            );
        }

        return $this->deserializeCredentials($credentialsDocument);
    }

    function delete(Credentials $credentials): void {
        $this->deleteDocument(
            $this->serializeCredentials($credentials)
        );
    }

    private function serializeCredentials(Credentials $credentials): array {
        return [
            self::FIELD_ID              => $credentials->getLogin(),
            self::FIELD_HASHED_PASSWORD => $credentials->getPasswordHash(),
        ];
    }

    private function serializeCredentialsPartWithId(Credentials $credentials): array {
        return [self::FIELD_ID => $credentials->getLogin()];
    }

    private function deserializeCredentials(array $credentialsDocument): Credentials {
        return new Credentials(
            $credentialsDocument[self::FIELD_ID],
            $credentialsDocument[self::FIELD_HASHED_PASSWORD],
        );
    }

    private function createDocumentPartWithLogin(string $login): array {
        return [self::FIELD_ID => $login];
    }

    private function isCredentialsDocumentValid(array $credentialsDocument): bool {
        return isset(
                $credentialsDocument[self::FIELD_ID],
                $credentialsDocument[self::FIELD_HASHED_PASSWORD],
            )
            && is_string($credentialsDocument[self::FIELD_ID])
            && is_string($credentialsDocument[self::FIELD_HASHED_PASSWORD])
        ;
    }
}
