<?php

declare(strict_types=1);

namespace Zalmoksis\User\Mongo;

use MongoDB\Driver\Query;
use Zalmoksis\User\{
    Exceptions\CorruptedDocument,
    Exceptions\DuplicateKey,
    Exceptions\DuplicateLogin,
    User,
    UserRepository,
};

final class UserMongoRepository extends MongoRepository implements UserRepository {
    protected const COLLECTION = 'users';

    private const FIELD_ID    = '_id';
    private const FIELD_ROLES = 'roles';

    function save(User $user): void {
        try {
            $this->insertDocument(
                $this->serializeUser($user)
            );
        } catch (DuplicateKey $exception) {
            throw new DuplicateLogin(
                login: $user->getLogin(),
                namespace: $exception->getNamespace(),
                previous: $exception
            );
        }
    }

    function update(User $user): void {
        $this->updateDocument(
            $this->serializeUserPartWithId($user),
            $this->serializeUser($user),
        );
    }

    function findByLogin(string $login): ?User {
        $cursor = $this->executeQuery(
            new Query($this->createUserDocumentPartWithLogin($login))
        );

        /** @var array | null $userDocument */
        $userDocument = $cursor->toArray()[0] ?? null;

        if (null === $userDocument) {
            return null;
        }

        if (!$this->isUserDocumentValid($userDocument)) {
            throw new CorruptedDocument(
                document: $userDocument,
                namespace: $this->database . '.' . self::COLLECTION,
            );
        }

        return $this->deserializeUser($userDocument);
    }

    function deleteByLogin(string $login): void {
        $this->deleteDocument(
            $this->createUserDocumentPartWithLogin($login)
        );
    }

    private function serializeUser(User $user): array {
        return [self::FIELD_ID => $user->getLogin()]
            + ($user->getRoles() ? [self::FIELD_ROLES => $user->getRoles()] : [])
        ;
    }

    private function serializeUserPartWithId(User $user): array {
        return [self::FIELD_ID => $user->getLogin()];
    }

    private function deserializeUser(array $userDocument): User {
        return new User(
            $userDocument[self::FIELD_ID],
            $userDocument[self::FIELD_ROLES] ?? [],
        );
    }

    private function createUserDocumentPartWithLogin(string $login): array {
        return [self::FIELD_ID => $login];
    }

    private function isUserDocumentValid(array $userDocument): bool {
        return isset($userDocument[self::FIELD_ID])
            && is_string($userDocument[self::FIELD_ID])
            && is_array($userDocument[self::FIELD_ROLES] ?? [])
            && $this->isArrayOfStrings($userDocument[self::FIELD_ROLES] ?? [])
        ;
    }

    private function isArrayOfStrings(array $fieldRoles): bool {
        return array_reduce(
            $fieldRoles,
            static fn(bool $carry, mixed $item) => $carry && is_string($item),
            initial: true
        );
    }
}
