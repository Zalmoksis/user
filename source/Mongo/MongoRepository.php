<?php

declare(strict_types=1);

namespace Zalmoksis\User\Mongo;

use LogicException;
use MongoDB\Driver\{BulkWrite, Cursor, Exception\BulkWriteException, Manager as Mongo, Query};
use Zalmoksis\User\Exceptions\DuplicateKey;

abstract class MongoRepository {
    protected const COLLECTION = '';

    final function __construct(
        protected Mongo $mongo,
        protected string $database,
    ) {
        if (!is_string(static::COLLECTION)) {
            throw new LogicException('Collection name in ' . __CLASS__ . ' is not string');
        }

        if (static::COLLECTION === '') {
            throw new LogicException('Undefined collection name in ' . __CLASS__);
        }
    }

    protected function insertDocument(array $document): void {
        $bulkWrite = new BulkWrite();
        $bulkWrite->insert($document);

        try {
            $this->executeBulkWrite($bulkWrite);
        } catch (BulkWriteException $exception) {
            if (str_starts_with($exception->getMessage(), 'E11000 ')) {
                throw new DuplicateKey(
                    document: $document,
                    namespace: $this->database . '.' . static::COLLECTION,
                    previous: $exception,
                );
            }

            throw $exception;
        }
    }

    protected function updateDocument(array $filter, array $document): void {
        $bulkWrite = new BulkWrite();
        $bulkWrite->update($filter, $document);

        $this->executeBulkWrite($bulkWrite);
    }

    protected function deleteDocument(array $filter): void {
        $bulkWrite = new BulkWrite();
        $bulkWrite->delete($filter);

        $this->executeBulkWrite($bulkWrite);
    }

    protected function executeBulkWrite(BulkWrite $bulkWrite): void {
        $this->mongo->executeBulkWrite(
            $this->database . '.' . static::COLLECTION,
            $bulkWrite
        );
    }

    protected function executeQuery(Query $query): Cursor {
        $cursor = $this->mongo->executeQuery(
            $this->database . '.' . static::COLLECTION,
            $query
        );

        $cursor->setTypeMap([
            'root'     => 'array',
            'document' => 'array',
        ]);

        return $cursor;
    }
}
