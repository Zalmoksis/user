<?php

declare(strict_types=1);

namespace Zalmoksis\User;

use LogicException;

final class Authenticator {
    function __construct(
        private CredentialsRepository $credentialsRepository,
    ) {}

    function storeCredentials(Credentials $credentials): void {
        $this->credentialsRepository->save(
            new HashedCredentials(
                $credentials->getLogin(),
                $this->hashPassword($credentials->getPassword())
            )
        );
    }

    function authenticate(Credentials $credentials): bool {
        $storedHashedCredentials = $this->credentialsRepository->findByLogin($credentials->getLogin());

        if (null === $storedHashedCredentials) {
            return false;
        }

        return password_verify($credentials->getPassword(), $storedHashedCredentials->getPasswordHash());
    }

    function updateCredentials(Credentials $credentials): void {
        $this->credentialsRepository->update(
            new HashedCredentials(
                $credentials->getLogin(),
                $this->hashPassword($credentials->getPassword())
            )
        );
    }

    private function hashPassword(string $password): string {
        return password_hash($password, PASSWORD_ARGON2I)
            ?: throw new LogicException('Unable to hash password');
    }
}
