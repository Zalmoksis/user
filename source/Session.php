<?php

declare(strict_types=1);

namespace Zalmoksis\User;

interface Session {
    function start(): self;
    function set(string $key, mixed $value): self;
    function has(string $key): bool;
    function get(string $key): mixed;
    function destroy(): self;
}
