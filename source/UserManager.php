<?php

declare(strict_types=1);

namespace Zalmoksis\User;

final class UserManager {
    function __construct(
        private Authenticator $authenticator,
        private Session $session,
        private UserRepository $userRepository,
    ) {}

    function initialize(): void {
        $this->session->start();
    }

    function addUser(Credentials $credentials, User $user): void {
        $this->authenticator->storeCredentials($credentials);
        $this->userRepository->save($user);
    }

    function logIn(Credentials $credentials): bool {
        if (!$this->authenticator->authenticate($credentials)) {
            return false;
        }

        $this->session->set('authenticated_user', $credentials->getLogin());

        return true;
    }

    function getAuthenticatedUser(): ?User {
        if (!$this->session->has('authenticated_user')) {
            return null;
        }

        return $this->userRepository->findByLogin(
            $this->session->get('authenticated_user')
        );
    }

    function updateCredentials(Credentials $credentials): void {
        $this->authenticator->updateCredentials($credentials);
    }

    function updateUser(User $user): void {
        $this->userRepository->update($user);
    }

    function logOut(): self {
        $this->session->destroy();

        return $this;
    }
}
