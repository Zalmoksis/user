<?php

declare(strict_types=1);

namespace Zalmoksis\User;

interface UserRepository {
    function save(User $user): void;
    function findByLogin(string $login): ?User;
    function update(User $user): void;
    function deleteByLogin(string $login): void;
}
