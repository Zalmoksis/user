<?php

declare(strict_types=1);

namespace Zalmoksis\User;

use Zalmoksis\User\HashedCredentials as Credentials;

interface CredentialsRepository {
    function save(Credentials $credentials): void;
    function findByLogin(string $login): ?Credentials;
    function update(Credentials $credentials): void;
    function delete(Credentials $credentials): void;
}
