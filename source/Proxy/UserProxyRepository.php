<?php

declare(strict_types=1);

namespace Zalmoksis\User\Proxy;

use Zalmoksis\User\{User, UserRepository};

class UserProxyRepository implements UserRepository {
    private array $users = [];

    function __construct(
        private UserRepository $userRepository,
    ) {}

    function save(User $user): void {
        $this->userRepository->save($user);
        $this->users[$user->getLogin()] = $user;
    }

    function findByLogin(string $login): ?User {
        if (!array_key_exists($login, $this->users)) { // null has to be cached
            $this->users[$login] = $this->userRepository->findByLogin($login);
        }

        return $this->users[$login];
    }

    function update(User $user): void {
        $this->userRepository->update($user);
        $this->users[$user->getLogin()] = $user;
    }

    function deleteByLogin(string $login): void {
        $this->userRepository->deleteByLogin($login);
        $this->users[$login] = null;
    }
}
