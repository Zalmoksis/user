<?php

declare(strict_types=1);

namespace Zalmoksis\User;

final class Credentials {
    function __construct(
        private string $login,
        private string $passwordHash,
    ) {}

    function getLogin(): string {
        return $this->login;
    }

    function getPassword(): string {
        return $this->passwordHash;
    }

    function changePassword(string $passwordHash): self {
        return new self($this->getLogin(), $passwordHash);
    }
}
