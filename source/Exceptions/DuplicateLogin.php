<?php

declare(strict_types=1);

namespace Zalmoksis\User\Exceptions;

use RuntimeException;
use Throwable;

final class DuplicateLogin extends RuntimeException implements UserException {

    function __construct(
        private string $login,
        private string $namespace,
        Throwable $previous,
    ) {
        parent::__construct(
            "Duplicate login '$login' in '$namespace'",
            previous: $previous,
        );
    }

    function getLogin(): string {
        return $this->login;
    }

    function getNamespace(): string {
        return $this->namespace;
    }
}
