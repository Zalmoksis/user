<?php

declare(strict_types=1);

namespace Zalmoksis\User\Exceptions;

use RuntimeException;
use Stringable;

final class CorruptedDocument extends RuntimeException implements UserException {

    function __construct(
        private array $document,
        private string $namespace,
    ) {
        parent::__construct(sprintf(
            "Corrupted document with _id: %s in '%s'",
            $this->representDocumentId($this->document),
            $this->namespace
        ));
    }

    function getDocument(): array {
        return $this->document;
    }

    function getNamespace(): string {
        return $this->namespace;
    }

    private function representDocumentId(array $document): string {
        if (!isset($document['_id'])) {
            return '(null)';
        }

        if (!is_string($document['_id']) && !$document['_id'] instanceof Stringable) {
            return '(non-string)';
        }

        return "'{$document['_id']}'";
    }
}
