<?php

namespace Zalmoksis\User\Exceptions;

use RuntimeException;
use Throwable;

class DuplicateKey extends RuntimeException {
    function __construct(
        private array $document,
        private string $namespace,
        Throwable $previous,
    ) {
        parent::__construct(
            "Duplicate key",
            previous: $previous
        );
    }

    public function getDocument(): array {
        return $this->document;
    }

    public function getNamespace(): string {
        return $this->namespace;
    }
}
