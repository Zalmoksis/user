# To do

## Bugfix (and refactoring)
- A wrapper for `password_*` functions

## Minor
- Cache for user
- No need to return ID
- Handling write errors

## Major
