# Changelog

All notable changes to this project will be documented in this file
in a format adhering to [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

The project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0).

## [0.6.5] — 2020-12-19
### Changed
- `CorruptedDocument` and `DuplicateLogin` now implement `UserException` interface.
  `CorruptedDocument` now contains the actual document.
  `DuplicateLogin` now contains the actual login.

## [0.6.4] — 2020-12-17
### Changed
- `CorruptedDocument` and `DuplicateLogin` now contain collection namespace.

## [0.6.3] — 2020-12-16
### Changed
- `CredentialsMongoRepository` and `UserMongoRepository` now extend a common abstract: `MongoRepository`.
### Fixed
- `CredentialsMongoRepository` and `UserMongoRepository` now more strictly validate the data from MongoDB
  and throw `CorruptedDocument` if the document is not valid.

## [0.6.2] — 2020-12-15
### Added
- Badges
### Changed
- Various improvements to the configuration of development environment

## [0.6.1] — 2020-12-12
### Changed
- Code and configuration formatting

## [0.6.0] — 2020-12-11
### Changed
- Interfaces `CredentialsRepository` and `UserRepository` have now all non-finding methods explicitly typed
  to return `void`.
- `CredentialsMongoRepository::save()` and `UserMongoRepository::save()` don't return login anymore. 
- Strong typing in `Session` interface.

## [0.5.2] — 2020-12-10
### Fixed
- `type` in `composer.json` is `library` now

## [0.5.1] — 2020-12-09
### Fixed
- Refactoring

## [0.5.0] — 2020-12-09
### Removed
- Compatibility with PHP 7.4

## [0.4.1] — 2020-12-09
### Added
- Allowing PHP 8.0

## [0.4.0] — 2020-01-12
### Added
- `UserManager::updateCredentials`
### Changed
- `Authenticator::changeCredentials` to `Authenticator::updateCredentials`
- Return values in `UserManager` and `Authenticator`

## [0.3.0] — 2020-01-01
### Added
- `UserProxyRepository` serving as a caching proxy
- Forgotten statement that this project adheres to Semantic Versioning
### Changed
- Renaming for consistency:
  - `MongoCredentialsRepository` to `CredentialsMongoRepository`
  - `MongoUserRepository` to `UserMongoRepository`
- Saving credentials and user doesn't return "id" anymore — there's no use for it.

## [0.2.1] — 2020-01-01
### Fixed
- This changelog

## [0.2.0] — 2020-01-01
### Added
- User is now represented by `User` object, may contain role tags
  and is stored in a separate repository interfaced by `UserRepository`.
- `MongoUserRepository` implementing `UserRepository`
- Coding standard test in Gitlab CI with failure allowed
- A little bit more tests
### Changed
- `UserService` is now `UserManager`.
- `UserService::getUser` is now `UserManager::getAuthenticatedUser`
  and returns `User` object.
- `StoredCredentials` are now `HashedCredentials`
  — hopefully it describes the content better
- `Session` is now an interface and the default implementation is renamed to `PhpSession`

## [0.1.0] — 2019-12-05
### Added
- Initial implementation; moved from [zalmoksis/web-dictionary](https://gitlab.com/Zalmoksis/web-dictionary)
